class Calculator:
    def add(x, y):
        return x + y
    def substract(x, y):
        return x - y
    def multiply(x, y):
        return x * y
    def divide(x, y):
        return x / y
    def power(x, y):
        result = 1
        for i in range(y):
            result *= x
        return result
    def square_root(x):
        if x == 0 or x == 1:
            return x
        val = x
        precision = 0.0000001
        while abs(val - x / val) > precision:
            val = (val + x / val) / 2
        return val
        
def calculate(operation, x, y):
    if operation == "add":
        result = Calculator.add(x,y)
    elif operation == "substract":
        result = Calculator.substract(x,y)
    elif operation == "multiply":
        result = Calculator.multiply(x,y)
    elif operation == "divide":
        result = Calculator.divide(x,y)
    elif operation == "power":
        result = Calculator.power(x,y)
    elif operation == "square_root":
        result = Calculator.square_root(x)
    return result

operation = input("Enter the operation you would like to perform (add, subtract, multiply, divide, square_root, power): ")
num1 = int(input("Enter the first number : "))
num2 = int(input("Enter the secod number : "))
print(calculate(operation, num1, num2))

import unittest

class TestCalculator(unittest.TestCase):
    def test_Add(self):
        nbr1 = 1
        nbr2 = 2
        result = Calculator.add(nbr1,nbr2)
        self.assertEqual(result, 3)
        self.assertEqual(Calculator.add(1.5, 1.5),3)

    def test_substract(self):
        nbr1 = 1
        nbr2 = 2
        result = Calculator.substract(nbr1,nbr2)
        self.assertEqual(result, -1)
        self.assertEqual(Calculator.substract(1.5, 1.5),0)

    def test_multiply(self):
        nbr1 = 1
        nbr2 = 2
        result = Calculator.multiply(nbr1,nbr2)
        self.assertEqual(result, 2)

    def test_divide(self):
        nbr1 = 4
        nbr2 = 2
        result = Calculator.divide(nbr1,nbr2)
        self.assertEqual(result, 2)

    def test_power(self):
        nbr1 = 4
        nbr2 = 2
        result = Calculator.power(nbr1,nbr2)
        self.assertEqual(result, 16)

    def test_square_root(self):
        result = Calculator.square_root(4)
        self.assertAlmostEqual(result, 2, delta=0.00001)

    def test_calculate_add(self):
        nbr1 = 1
        nbr2 = 2
        result = calculate("add", nbr1, nbr2)
        self.assertEqual(result, 3)

    def test_calculate_substract(self):
        nbr1 = 1
        nbr2 = 2
        result = calculate("substract", nbr1, nbr2)
        self.assertEqual(result, -1)

    def test_calculate_multiply(self):
        nbr1 = 1
        nbr2 = 2
        result = calculate("multiply", nbr1, nbr2)
        self.assertEqual(result, 2)

    def test_calculate_divide(self):
        nbr1 = 4
        nbr2 = 2
        result = calculate("divide", nbr1, nbr2)
        self.assertEqual(result, 2)

    def test_calculate_power(self):
        nbr1 = 4
        nbr2 = 2
        result = calculate("power", nbr1, nbr2)
        self.assertEqual(result, 16)

    def test_calculate_square_root(self):
        result = calculate("square_root", 4, 4)
        self.assertAlmostEqual(result, 2, delta=0.00001)
        result =  calculate("square_root", -4, 4)
        self.assertAlmostEqual(result, 2, delta=0.00001)
        
if __name__ == '__main__':
    unittest.main()
    