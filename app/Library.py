class Book:
    def __init__(self, title, author):
        self.title = title
        self.author = author
        self.is_checked_out = False
    def check_out(self):
        self.is_checked_out = True
        print(f"{self.title} by {self.author} has been checked out.")
    def check_in(self):
        self.is_checked_out = False
        print(f"{self.title} by {self.author} has been checked in.")
        
class Library:
    def __init__(self):
        self.books = []
    def add_book(self, book):
        self.books.append(book)
        print(f"{book.title} by {book.author} has been added to the library.")
    def check_out_book(self, title):
        for book in self.books:
            if book.title == title and not book.is_checked_out:
                book.check_out()
                return
        print(f"Sorry, {title} is not available.")
    def check_in_book(self, title):
        for book in self.books:
            if book.title == title and book.is_checked_out:
                book.check_in()
                return
        print(f"Sorry, {title} is not in the library.")

class Client:
    def __init__(self, name):
        self.name = name
        self.checked_out_books = []
    def check_out_book(self, library, title):
        for book in library.books:
            if book.title == title and not book.is_checked_out:
                book.check_out()
                self.checked_out_books.append(book)
                return
        print(f"Sorry, {title} is not available.")
    def check_in_book(self, title):
        for book in self.checked_out_books:
            if book.title == title:
                book.check_in()
                self.checked_out_books.remove(book)
                return
        print(f"Sorry, {title} is not checked out.")

library = Library()
book1 = Book("To Kill a Mockingbird", "Harper Lee")
library.add_book(book1)
book2 = Book("Pride and Prejudice", "Jane Austen")
library.add_book(book2)
client1 = Client("John Doe")
client1.check_out_book(library, "To Kill a Mockingbird")
client1.check_out_book(library, "Pride and Prejudice")
client2 = Client("Jane Doe")
client2.check_out_book(library, "To Kill a Mockingbird")
client1.check_in_book("To Kill a Mockingbird")

import unittest

class TestLibrary(unittest.TestCase):
    library = Library()
    book = Book("Oui oui", "Harper Lee")
    client = Client("Océanne Pandore")
    book2 = Book("Non non", "Harper Lee")

    def test_book_check_out(self):
        book = Book("Oui oui", "Harper Lee")
        book.check_out
        self.assertEqual(book.title, "Oui oui")
        self.assertTrue(book.is_checked_out)

    def test_book_check_in(self):
        book = Book("Oui oui", "Harper Lee")
        book.check_in
        self.assertFalse(book.is_checked_out)

    def test_library_add_book(self):
        library = Library()
        book = Book("Oui oui", "Harper Lee")
        library.add_book(book)
        self.assertEqual(len(library.books), 1)
        self.assertEqual(library.books[0], book)
    
    def test_library_check_out_book(self):
        library = Library()
        book = Book("Oui oui", "Harper Lee")
        library.check_out_book("Oui oui")
        self.assertTrue(book.is_checked_out)

    def test_library_check_in_book(self):
        library = Library()
        book = Book("Oui oui", "Harper Lee")
        library.check_in_book("Oui oui")
        self.assertFalse(book.is_checked_out)

    def test_client_check_out_book(self):
        library = Library()
        book = Book("Oui oui", "Harper Lee")
        client = Client("Océanne Pandore")
        library.add_book(book)
        self.assertEqual(len(library.books), 1)
        client.check_out_book(library, "Oui oui")
        self.assertEqual(len(client.checked_out_books), 1)

    def test_client_check_in_book(self):
        library = Library()
        client = Client("Océanne Pandore")
        book2 = Book("Non non", "Harper Lee")
        library.add_book(book2)
        self.assertEqual(len(library.books), 1)
        client.check_in_book("Non non")
        self.assertEqual(len(client.checked_out_books), 0)